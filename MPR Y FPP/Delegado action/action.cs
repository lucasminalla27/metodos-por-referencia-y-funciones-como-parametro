﻿using System;

namespace Delegado_action
{
    class Program
    {
        static void funcionparapasar(int balones)
        {
            int Incrementar = balones + 1;
            Console.WriteLine("Total de balones en el sitema = " + balones);
            Console.WriteLine("Total actualizado  de balones en el sistema = " + Incrementar);
        }
        static void Tienda(Action<int> funcionparapasar)
        {
            funcionparapasar(10);
        }
        static void Main(string[] args)
        {
            Tienda(funcionparapasar);
        }
    }
}

﻿using System;

namespace Delegado_Func
{
    class Program
    {

        static int Funcionparapasar(int balones)
        {
            return balones + 10;
        }
        static void Tienda(Func<int, int> Funcionparapasar)
        {
            int total = Funcionparapasar(42);
            Console.WriteLine("Total actualizado de balones: " + total);
        }
        static void Main(string[] args)
        {
            Tienda(Funcionparapasar);
        }
    }
}

﻿using System;

namespace MPR_Y_FPP
{
    class Tienda

    {

        public int balon { get; set; }

    }
    class Program
    {
        public static void Main(String[] args)
        {
            // usamos referencia por referencia 
            {
                //INSTANCIAMOS AL OBJETO TIENDA 
                var inicial = new Tienda();

                //
                inicial.balon = 10;

                Incrementa(ref inicial);

                Console.WriteLine("EL Valor DESPUES de incrementar 1 balones es " + inicial.balon);

                Console.ReadLine();

            }

            static void Incrementa(ref Tienda tienda)

            {

                int valor = tienda.balon;

                tienda = new Tienda();

                tienda.balon = valor + 1;


            }

            //*Usamos referencia por datos
            {
                var inicia2 = 4;

                Incrementa2(ref inicia2);

                Console.WriteLine("Valor DESPUES de incrementar es " + inicia2);
            }



            static void Incrementa2(ref int valor2)

            {

                valor2 = valor2 + 1;

            }

        }
    }
}
